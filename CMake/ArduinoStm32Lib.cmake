SET(CCLL                                "CMAKE_CURRENT_LIST_LINE")


#---  Add Include Directories  ---#

#include_directories(${SKETCH_PATH})
include_directories(${APP_PATH})
include_directories(${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT})
include_directories(${CORE_PATH}/cores/arduino)
include_directories(${CORE_PATH}/cores/arduino/avr)
include_directories(${CORE_PATH}/cores/arduino/stm32)
include_directories(${CORE_PATH}/cores/arduino/stm32/LL)
include_directories(${CORE_PATH}/cores/arduino/stm32/usb)
include_directories(${CORE_PATH}/cores/arduino/stm32/usb/hid)
include_directories(${CORE_PATH}/cores/arduino/stm32/usb/cdc)
include_directories(${CORE_PATH}/system/Drivers/${BUILD_VARIANT_SERIES}_HAL_Driver/Inc/)
include_directories(${CORE_PATH}/system/Drivers/${BUILD_VARIANT_SERIES}_HAL_Driver/Src/)
include_directories(${CORE_PATH}/system/Drivers/CMSIS/Device/ST/${BUILD_VARIANT_SERIES}/Include/)
include_directories(${CORE_PATH}/system/Drivers/CMSIS/Device/ST/${BUILD_VARIANT_SERIES}/Source/Templates/gcc/)
include_directories(${CORE_PATH}/system/${BUILD_VARIANT_SERIES}/)
include_directories(${CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc)
include_directories(${CORE_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src)
include_directories(${CORE_PATH}/libraries/SrcWrapper/src)
include_directories(${CMSIS_PATH}/CMSIS/Core/Include/)
include_directories(${CMSIS_PATH}/CMSIS/DSP/Include/)


#--- Add Sources  ---#

file(GLOB_RECURSE APP_SOURCES                   ${APP_PATH}/${PROJECT_NAME}.cpp)
file(GLOB_RECURSE APP_SOURCES2                  ${APP_PATH}/SrcWrapper.cpp)
file(GLOB_RECURSE VARIANTS_CPP_SOURCES          ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.cpp)
file(GLOB_RECURSE VARIANTS_C_SOURCES            ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.c)
file(GLOB_RECURSE STARTUP_SOURCES               ${CORE_PATH}/variants/${BUILD_VARIANT_SERIES}/${MCU_PRODUCT}/*.S)
#file(GLOB_RECURSE STARTUP_SOURCES               ${CORE_PATH}/cores/arduino/stm32/*.S)
file(GLOB_RECURSE ARDUINO_C_SOURCES             ${CORE_PATH}/cores/arduino/*.c)
file(GLOB_RECURSE ARDUINO_CPP_SOURCES           ${CORE_PATH}/cores/arduino/*.cpp)
file(GLOB_RECURSE AVR_C_SOURCES                 ${CORE_PATH}/cores/arduino/avr/*.c)
file(GLOB_RECURSE STM32_USB_C_SOURCES           ${CORE_PATH}/cores/arduino/stm32/usb/*.c)
file(GLOB_RECURSE STM32_USB_HID_C_SOURCES       ${CORE_PATH}/cores/arduino/stm32/usb/hid/*.c)
file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES       ${CORE_PATH}/cores/arduino/stm32/usb/cdc/*.c)
file(GLOB_RECURSE SRCWRAPPER_C_SOURCES          ${CORE_PATH}/libraries/SrcWrapper/*.c)
file(GLOB_RECURSE SRCWRAPPER_HAL_C_SOURCES    	${CORE_PATH}/libraries/SrcWrapper/src/HAL/*.c)
file(GLOB_RECURSE SRCWRAPPER_LL_C_SOURCES    	${CORE_PATH}/libraries/SrcWrapper/src/LL/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_C_SOURCES    ${CORE_PATH}/libraries/SrcWrapper/src/stm32/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_CPP_SOURCES  ${CORE_PATH}/libraries/SrcWrapper/src/stm32/*.cpp)


set(ARDUINOLIB_SOURCES   ${VARIANTS_CPP_SOURCES} ${VARIANTS_C_SOURCES} ${STARTUP_SOURCES} ${ARDUINO_CPP_SOURCES} ${ARDUINO_C_SOURCES} ${AVR_C_SOURCES} ${STM32_USB_C_SOURCES} ${STM32_USB_HID_C_SOURCES})
set(ARDUINOLIB_SOURCES   ${ARDUINOLIB_SOURCES} ${STM32_USB_CDC_C_SOURCES} ${SRCWRAPPER_C_SOURCES} ${SRCWRAPPER_HAL_C_SOURCES} ${SRCWRAPPER_LL_C_SOURCES} ${SRCWRAPPER_STM32_C_SOURCES} ${SRCWRAPPER_STM32_CPP_SOURCES}) 


#set(APP_SOURCES     ${SKETCH_CPP_SOURCES} ${MCU_LINKER_SCRIPT} ${SYS_LINKER_SCRIPT})
 
#---------------  All Done  ---------------------------------------------------------------------------------------------------#

