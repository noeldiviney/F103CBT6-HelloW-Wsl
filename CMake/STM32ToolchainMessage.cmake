SET(CCLL CMAKE_CURRENT_LIST_LINE)
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}     --------------------------------------------------------------------------------------------------------------------------------------------")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}     Entering                         STM32Toolchain.cmake")


MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_NAME              = ${CMAKE_SYSTEM_NAME}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_VERSION           = ${CMAKE_SYSTEM_VERSION}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}     CMAKE_SYSTEM_PROCESSOR         = ${CMAKE_SYSTEM_PROCESSOR}")


MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_COMPILER             = ${CMAKE_CXX_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_COMPILER             = ${CMAKE_ASM_COMPILER}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJCOPY                  = ${CMAKE_OBJCOPY}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_OBJDUMP                  = ${CMAKE_OBJDUMP}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_AR                       = ${CMAKE_AR}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_TRY_COMPILE_TARG_TYPE    = ${CMAKE_TRY_COMPILE_TARGET_TYPE}")

MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    MCU_SERIES_TYPE                = ${MCU_SERIES_TYPE}")

MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS                  = ${CORE_FLAGS}  ")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS Contd            = --specs=nano.specs --specs=nosys.specs")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_C_FLAGS Contd            = -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2")



MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS1   = ${CORTEX_FLAGS} -mthumb -mlittle-endian -mthumb-interwork")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs ${ADDITIONAL_CORE_FLAGS}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_CXX_FLAGS  C_FLAGS       = -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS1   = ${CMAKE_ASM_FLAGS}")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  CORE_FLAGS2   = --specs=nano.specs --specs=nosys.specs")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    CMAKE_ASM_FLAGS  C_FLAGS       = -g -ggdb3 -D__USES_CXX")

MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    Leaving                          STM32Toolchain.cmake")
MESSAGE(STATUS "Stm32ToolChain.cmake  line ${${CCLL}}    --------------------------------------------------------------------------------------------------------------------------------------------")

